// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   playground.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/13 11:42:23 by scornaz           #+#    #+#             //
//   Updated: 2018/06/19 19:05:24 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <stdio.h>
#include "Music.hpp"
#include "Renderer.hpp"
#include "hot_reload.hpp"
#include <GLFW/glfw3.h>

void	set_callbacks (GLFWwindow* window, int key, int scancode,
					   int action, int mods)
{
	Music* music = (Music*)glfwGetWindowUserPointer(window);

	// printf("%s\n", action == GLFW_RELEASE ? "release" :
	// 	   (action == GLFW_PRESS ? "press" :
	// 		(action == GLFW_REPEAT ? "repeat" : 0)));
	// fflush(stdout);
	if (action == GLFW_RELEASE)
		return ;
	switch (key)
	{
	case GLFW_KEY_E :
		music->send_osc("/include", "s", "open.scd");
		break;
	case GLFW_KEY_Q :
		music->send_osc("/stop");
		break;
	}
}

void  	err_fun(int err, const char *str)
{
	printf("ERROR: %d & %s\n", err, str);
}

int 	main()
{
	Music		music(57120);
	Renderer	render(800, 1200, &music);
	Daemon		hot(&render, "shaders/vertexShader.glsl",
					"shaders/fragmentShader.glsl");
	render.open();
	hot.start();
	glfwSetWindowUserPointer(render.window, &music);
	glfwSetKeyCallback(render.window, set_callbacks);
	glfwSetErrorCallback(err_fun);
	glfwSwapInterval(1);
	render.draw();
	glfwDestroyWindow(render.window);
    glfwTerminate();
	return (0);
}
