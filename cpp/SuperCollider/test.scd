(
var path = "/users/scornaz/openGL/SuperCollider/";
"Port is %".format(NetAddr.langPort).postln;
"running in %".format(thisProcess.nowExecutingPath).postln;

//OSCFunc.trace;

o = Server.local.options;
o.numInputBusChannels = 0;

OSCdef(\include, { arg a;
(path ++ "include.scd").loadPaths[0].value(path ++ a[1])
}, \include);
OSCdef(\stop, {
	"stop".postln;
	~player.stop;
	~synths.do({ |synth| synth.postln; ~synths.remove(synth.release) });
	~effect.release;
// Execute after sound has completely died away.
//~effectBus.free;
	// )
//	  CmdPeriod.run
}, \stop);
OSCdef(\changed, {
	 "changed".postln;
}, \changed);
s.waitForBoot{
	"open.scd".loadRelative;
	128.do{ arg i;
		[\play, \stop].do{ arg cmd;
			var name = cmd ++ '_' ++ i;
			OSCdef(name, {
				Pdef(i.asSymbol).perform(cmd);
			}, name);
		};
		Pdef(i.asSymbol,
			PbindOSC(i, \degree, 20.rand2, \amp, 0)
		)
	}
}

)