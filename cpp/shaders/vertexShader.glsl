#version 330 core

uniform mat4 MVP;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 uvs;
layout(location = 2) in vec3 normals_modelspace;
out vec2 UVS;
out vec3 NORMALS;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;

void main(){
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
	vec3 vertexPosition_cameraspace = (view * model *
									   vec4(vertexPosition_modelspace, 1)).xyz;
	eyeDirection_cameraspace = vec3(0, 0, 0) - vertexPosition_cameraspace;
	lightDirection_cameraspace = (view * vec4(0, 20, 0, 1)).xyz
		+ eyeDirection_cameraspace;
	UVS = uvs;
	NORMALS = (view * vec4(normals_modelspace, 0)).xyz;
}
