#version 330 core

uniform sampler2D my_texture;
in vec3 lightDirection_cameraspace;
in vec3 NORMALS;
in vec2 UVS;
out vec3 color;

void main(){
	float cosTheta = clamp(dot(normalize(NORMALS), normalize(lightDirection_cameraspace)), 0, 1);
	color = texture(my_texture, UVS).rgb * vec3(1, 1, 1) * (normalize(NORMALS) * cosTheta );
}
