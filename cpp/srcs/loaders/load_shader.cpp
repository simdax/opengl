#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include "Shader.hpp"

int		get_code(std::string *VertexShaderCode, const char *vertex_file_path)
{
	// Read the Vertex Shader code from the file

	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open())
		{
			std::stringstream sstr;
			sstr << VertexShaderStream.rdbuf();
			*VertexShaderCode = sstr.str();
			VertexShaderStream.close();
		}
	else
		{
			printf("Impossible to open %s. Are you in the right directory ? \n",
						 vertex_file_path);
			getchar();
			return (0);
		}
	return (1);
}

void	get_log(int *InfoLogLength, GLint *Result, GLuint VertexShaderID)
{
	std::vector<char> VertexShaderErrorMessage(*InfoLogLength + 1);
	glGetShaderInfoLog(VertexShaderID, *InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	printf("error %d with compilation: %s\n", *Result, &VertexShaderErrorMessage[0]);
}

int		compile_shader(std::string ShaderCode, const char *file_path,
										 GLuint ShaderID, GLint *Result, int *InfoLogLength)
{
	// Compile  Shader
	char const *SourcePointer = ShaderCode.c_str();
	printf("Compiling shader : %s\n", file_path);
#ifdef DEBUG
	printf("\x1b[32m %s\x1b[0m\n", SourcePointer);
#endif
	glShaderSource(ShaderID, 1, &SourcePointer, NULL);
	glCompileShader(ShaderID);
	// Check  Shader
	glGetShaderiv(ShaderID, GL_COMPILE_STATUS, Result);
	glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, InfoLogLength);
	if ( *InfoLogLength > 0 )
		{
			get_log(InfoLogLength, Result, ShaderID);
			return (0);
		}
	return (1);
}

GLuint Shader::load(const char *vertex_file_path, const char *fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	std::string VertexShaderCode;
	std::string FragmentShaderCode;
	GLint Result = GL_FALSE;
	int InfoLogLength = 0;

	if (!VertexShaderID || !FragmentShaderID)
		{
			std::cout << glGetError() << std::endl;
			return(0);
		}
	if (!get_code(&VertexShaderCode, vertex_file_path) ||
			!get_code(&FragmentShaderCode, fragment_file_path) )
		return (0);
	if (!compile_shader(VertexShaderCode, vertex_file_path,
											VertexShaderID, &Result, &InfoLogLength) ||
			!compile_shader(FragmentShaderCode, fragment_file_path,
											FragmentShaderID, &Result, &InfoLogLength) )
		return (0);
	// Link the program
	printf("Linking program with ids : %d %d\n", VertexShaderID, FragmentShaderID);
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);
	// Check the program
	printf("Checking program %d\n", ProgramID);
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		get_log(&InfoLogLength, &Result, ProgramID);
		return (0);
	}
	//free
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);
	return (ProgramID);
}
