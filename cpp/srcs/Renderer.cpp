#include "Renderer.hpp"
#include "openwindow.hpp"
#include "matrix.hpp"
#include "mouse.hpp"
#include "Object.hpp"

Renderer::Renderer(unsigned width, unsigned height, Music *m) : music(m)
{
	w = width;
	h = height;
}

int	Renderer::open()
{
	if (open_window(&window, w, h))
		return (-1); //todo : check errors
	shaders = new Shader("shaders/1_v.glsl",
						 "shaders/1_f.glsl");
	glGenVertexArrays(1, &VertexArrayID);
 	glBindVertexArray(VertexArrayID);
	return (0);
}

void	Renderer::check_hot_reload()
{
	if (must_recompile)
	{
		Shader *s = new Shader("shaders/1_v.glsl",
							   "shaders/1_f.glsl");
		if (s->program)
			shaders = s;
		must_recompile = false;
		music->send_osc("/changed");
	}
}

void	Renderer::gl_flags()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LESS);
}

void	Renderer::draw()
{
	View				camera(0, 0, 5, 0, 0, 0, 0, 1, 0);
	Projection  		projection(70, w, h);
	Projection_Ortho	ortho(-20.0f, 10.0f, -10.0f, 15.0f, 0.0f, 100.0f);
	Mouse				mouse(w, h, window, glm::vec3(0, 0, 5));

	printf("openGL version : %s\n", glGetString(GL_VERSION));
	printf("GLFW version : %s\n", glfwGetVersionString());
	Object object("blender/mountains.obj", &projection, &camera,
				  shaders, "material/roc.bmp");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, w / 2, h / 2);
	do {
		this->check_hot_reload();
		this->gl_flags();
		mouse.move(&camera.matrix);
		object.draw(shaders);
		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
			 glfwWindowShouldClose(window) == 0);
}
