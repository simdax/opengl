#ifndef OBJECT_H
#define OBJECT_H

#include "matrix.hpp"
#include "Shader.hpp"
#include <GL/glew.h>
#include "Texture.hpp"

class Object {
private:
	const Projection 	*proj;
  const View				*camera;
  const Shader			*shader;
	GLuint		vertices;
	GLuint		uvs;
	GLuint		normals;
	unsigned	size_vertices;
	Texture		*texture;
public:
	glm::mat4	position;
	Object(const char *obj_path, const Projection *proj_, const View *camera_,
				 const Shader	*shader_, const char *texture_path);
	void	draw(Shader *s);
	void	draw();
};

#endif //OBJECT_H
