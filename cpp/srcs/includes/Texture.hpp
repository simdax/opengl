#ifndef TEXTURE_H
#define TEXTURE_H
#include GL

class Texture {
private:
	GLuint	TextureData;
  GLuint	TextureID;
  GLuint	uvBuffer;
public:
	Texture(const GLfloat *data, unsigned data_size,
					GLuint programID, const char *path);
	void	activate(unsigned index);
};

#endif //TEXTURE_H
