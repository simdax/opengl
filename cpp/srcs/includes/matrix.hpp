#ifndef MATRIX_H
#define MATRIX_H

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

class My_Matrix {
public:
	glm::mat4 matrix;
	void	print();
};

class Projection : public My_Matrix
{
public:
	Projection(float fov = 70, float width = 500, float height = 500,
			   float near = 0.1, float far = 100);
};

class Projection_Ortho : public My_Matrix
{
public:
	Projection_Ortho(float x1, float x2, float y1, float y2, float z1, float z2);
};

class View : public My_Matrix
{
private:
	glm::vec3	base_position;
public:
	View(float from1, float from2, float from3,
		 float to1, float to2, float to3,
			 float head1, float head2, float head3);
	void	calc_mouse();
};

#endif //MATRIX_H
