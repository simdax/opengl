#ifndef SHADER_H
#define SHADER_H
#include <GL/glew.h>

class Shader {
private:
	GLuint load(const char * vertex_file_path, const char * fragment_file_path);
public:
	GLuint	program;
	GLuint	m;
	GLuint	v;
	GLuint	p;
	GLuint  matrix;
	GLuint  light;
	GLuint  texture;
	Shader(const char *vert_shader, const char *frag_shader);
	void	print();
};

#endif //SHADER_H
