// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Renderer.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 18:31:10 by scornaz           #+#    #+#             //
//   Updated: 2018/06/19 16:47:08 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef RENDERER_H
#define RENDERER_H

#include "Shader.hpp"
#include "Music.hpp"
#include <GLFW/glfw3.h>

class Renderer
{
public:
	GLuint		program;
	bool		must_recompile = false;
	int			open();
	void		create_shaders();
	void		draw();
	GLFWwindow	*window;
	Shader		*shaders;
	Music		*music;
	Renderer(unsigned width, unsigned height, Music *m);

private:
	void		check_hot_reload();
	void  		gl_flags();
	GLuint		VertexArrayID;
	unsigned	w;
	unsigned	h;
};

#endif //RENDERER_H
