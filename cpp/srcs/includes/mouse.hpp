#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Mouse {
private:
	unsigned	mouse_reset;
	float 		center_width;
	float 		center_height;
	float		horizontalAngle;
	float		verticalAngle;
	float 		speed;
	float 		mouseSpeed;
	GLFWwindow	*window;
	glm::vec3	position;
	float		lastTime;
public:
	Mouse(int w, int h, GLFWwindow *window, glm::vec3 position);
	void	move(glm::mat4 *mat);
};
