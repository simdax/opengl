#include GL

void	configure_buffer(GLuint buffer, int glsl_id, unsigned size = 3);
GLuint	create_buffer(int type, const GLfloat *data, unsigned size);
