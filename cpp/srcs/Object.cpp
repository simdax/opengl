// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Object.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 10:50:44 by scornaz           #+#    #+#             //
//   Updated: 2018/06/18 18:02:52 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Object.hpp"
#include "draw.hpp"
#include "objloader.hpp"
#include <iostream>

Object::Object(const char *obj_path,
			   const Projection *proj_, const View	*camera_,
			   const Shader *shader_, const char *texture_path)
{
	std::vector<glm::vec3> out_vertices;
	std::vector<glm::vec2> out_uvs;
	std::vector<glm::vec3> out_normals;
	std::vector<unsigned short> indices;

	loadAssImp(obj_path, indices, out_vertices, out_uvs, out_normals);
	position = glm::mat4(1.0);
	proj = proj_;
	camera = camera_;
	shader = shader_;
	size_vertices = out_vertices.size();
	vertices = create_buffer(0, &out_vertices[0][0],
							 size_vertices * 3 * sizeof(GLfloat));
	normals = create_buffer(0, &out_normals[0][0],
							out_normals.size() * 3 * sizeof(GLfloat));
	texture = new Texture(&out_uvs[0][0],
						  out_uvs.size() * 2 * sizeof(GLfloat),
						  shader->program, texture_path);
}

void	Object::draw(Shader *s)
{
	glm::mat4 mvp = proj->matrix * camera->matrix * position;
	glm::vec3 l = glm::vec3(0, 0, 5);

	glUseProgram(s->program);
	glUniform3f(shader->light, l.x, l.y, l.z);
	glUniformMatrix4fv(shader->matrix, 1, GL_FALSE, &mvp[0][0]);
	glUniformMatrix4fv(shader->m, 1, GL_FALSE, &position[0][0]);
	glUniformMatrix4fv(shader->v, 1, GL_FALSE, &camera->matrix[0][0]);
	glUniformMatrix4fv(shader->p, 1, GL_FALSE, &proj->matrix[0][0]);
	configure_buffer(vertices, 0, 3);
	texture->activate(1);
	configure_buffer(normals, 2, 3);
	glDrawArrays(GL_TRIANGLES, 0, size_vertices);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}
