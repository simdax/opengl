#include <GL/glew.h>
#include "draw.hpp"
#include "Texture.hpp"
#include "bpm.hpp"
#include <stdio.h>

Texture::Texture(const GLfloat *data, unsigned data_size, GLuint programID,
								 const char *path)
{
	uvBuffer = create_buffer(1, data, data_size);
	TextureData = loadBMP_custom(path);
}

void	Texture::activate(unsigned index)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureData);
	glUniform1i(TextureID, 0);
	configure_buffer(uvBuffer, index, 2);
}
