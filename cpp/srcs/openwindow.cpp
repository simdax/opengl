#include <GL/glew.h>
#include <GLFW/glfw3.h>

enum	errors		 		{NO_GLFW, NO_GLEW, NO_WINDOW};
enum	buffer_types	{VERTICES, COLORS};

int		open_window(GLFWwindow **window, unsigned w, unsigned h)
{
	if (!glfwInit())
		return (NO_GLFW);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	*window = glfwCreateWindow(w, h, "coucou", 0, 0);
	if (!window)
		return (NO_WINDOW);
	glfwMakeContextCurrent(*window);
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
		return (NO_GLEW);
	return (0);
}
