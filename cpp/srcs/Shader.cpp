// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Shader.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 11:19:37 by scornaz           #+#    #+#             //
//   Updated: 2018/06/18 11:19:41 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Shader.hpp"
#include <GL/glew.h>
#include <iostream>

Shader::Shader(const char *vert_shader, const char *frag_shader)
{
	program = load(vert_shader, frag_shader);
	if (!program)
		std::cout << "error in shader loading" << std::endl;
	else
		{
			glUseProgram(program);
			matrix = glGetUniformLocation(program, "MVP");
			m = glGetUniformLocation(program, "model");
			v = glGetUniformLocation(program, "view");
			p = glGetUniformLocation(program, "projection");
			light = glGetUniformLocation(program, "LightPosition_worldspace");
			texture = glGetUniformLocation(program, "my_texture");
			this->print();
		}
}

void	Shader::print()
{
	std::cout << "shader nb : " << program << std::endl;
	std::cout << matrix << std::endl;
	std::cout << texture << std::endl;
	std::cout << m << " " << v << " " << p << " " << std::endl;
}
