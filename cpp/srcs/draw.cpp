#include <GL/glew.h>
#include <iostream>

GLuint	create_buffer(int type, const GLfloat *data, unsigned size)
{
	GLuint	buffer;

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	return (buffer);
}

void	configure_buffer(GLuint buffer, int glsl_id, unsigned size = 3)
{
	glEnableVertexAttribArray(glsl_id);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glVertexAttribPointer(glsl_id, size, GL_FLOAT, GL_FALSE, 0, (void*)0);
}
