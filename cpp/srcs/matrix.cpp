#include <glm/gtc/matrix_transform.hpp>
#include "matrix.hpp"

void My_Matrix::print() {
	std::cout << "View matrix : \n{"
			  << matrix[0][0] << " " << matrix[0][1] << " " << matrix[0][2] << std::endl
			  << matrix[1][0] << " " << matrix[1][1] << " " << matrix[1][2] << std::endl
			  << matrix[2][0] << " " << matrix[2][1] << " " << matrix[2][2] << "}" << std::endl;
}

Projection::Projection(float fov, float width, float height,
					   float near, float far)
{
	matrix = glm::perspective(glm::radians(fov), width / height, near, far);
}

Projection_Ortho::Projection_Ortho(float x1, float x2, float y1, float y2, float z1, float z2)
{
	matrix = glm::ortho(x1, x2, y1, y2, z1, z2);
}

View::View(float from1, float from2, float from3,
		   float to1, float to2, float to3,
		   float head1, float head2, float head3)
{
	matrix = glm::lookAt(glm::vec3(from1, from2, from3),
						 glm::vec3(to1, to2, to3),
						 glm::vec3(head1, head2, head3));
}
