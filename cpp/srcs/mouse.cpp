// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   mouse.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 10:51:59 by scornaz           #+#    #+#             //
//   Updated: 2018/06/19 15:58:56 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "mouse.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <iostream>

Mouse::Mouse(int w, int h, GLFWwindow *win, glm::vec3 position_)
{
	mouse_reset = 10;
	center_width = (float)w / 2;
	center_height = (float)h / 2;
	window = win;
	horizontalAngle = 3.14;
	verticalAngle = 0.0;
	speed = 3.0f; // 3 units / second
	mouseSpeed = 0.005f;
	position = position_;
	lastTime = 0;
}

void	Mouse::move(glm::mat4 *matrix)
{
	double		xpos;
	double		ypos;
	glm::vec3	direction;
	glm::vec3	right;
	glm::vec3	up;

	float now = glfwGetTime();
	float deltaTime = now - lastTime;
	glfwGetCursorPos(window, &xpos, &ypos);
	glfwSetCursorPos(window, center_width, center_height);
	horizontalAngle += mouseSpeed * (center_width - xpos);
	verticalAngle += mouseSpeed * (center_height - ypos);
	direction = glm::vec3(cos(verticalAngle) * sin(horizontalAngle),
						  sin(verticalAngle),
						  cos(verticalAngle) * cos(horizontalAngle));
	right = glm::vec3(sin(horizontalAngle - 3.14 / 2),
					  0,
					  cos(horizontalAngle - 3.14 / 2));
	up = glm::cross(right, direction);
	if (glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS){
		position += direction * deltaTime * speed;
	}
	if (glfwGetKey(window,  GLFW_KEY_S ) == GLFW_PRESS){
		position -= direction * deltaTime * speed;
	}
	if (glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS){
		position += right * deltaTime * speed;
	}
	if (glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS){
		position -= right * deltaTime * speed;
	}
	*matrix = glm::lookAt(position, position + direction, up);
	lastTime = now;
}
