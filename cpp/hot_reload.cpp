// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   hot_reload.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 12:15:17 by scornaz           #+#    #+#             //
//   Updated: 2018/06/18 13:21:41 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "hot_reload.hpp"

Daemon::Daemon(Renderer *r, const char *v1, const char *v2)
	: renderer(r), path_vertex(v1), path_frag(v2)
{
}

void	Daemon::start() {
	std::thread t(&Daemon::run, this);
	t.detach();
};

void	Daemon::run()
{
	unsigned	time_v = 0;
	unsigned	time_f = 0;
	unsigned	last_v = 0;
	unsigned	last_f = 0;

	stat("shaders/1_f.glsl", &file_infos);
	last_f = file_infos.st_mtime;
	stat("shaders/1_v.glsl", &file_infos);
	last_v = file_infos.st_mtime;
	while (true)
	{
		stat("shaders/1_f.glsl", &file_infos);
		time_f = file_infos.st_mtime;
		stat("shaders/1_v.glsl", &file_infos);
		time_v = file_infos.st_mtime;
		if (time_v > last_v || time_f > last_f)
		{
			printf("reloading shader\n");
			fflush(stdout);
			renderer->must_recompile = true;
		}
		last_v = time_v;
		last_f = time_f;
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}
