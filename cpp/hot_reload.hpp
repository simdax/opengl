// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   hot_reload.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2018/06/18 16:53:06 by scornaz           #+#    #+#             //
//   Updated: 2018/06/18 16:53:15 by scornaz          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <thread>
#include <chrono>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "Renderer.hpp"

class Daemon {
public:
	Daemon(Renderer *r, const char *v1, const char *v2);
	void start();

private:
	Renderer	*renderer;
	const char	*path_vertex;
	const char	*path_frag;
	struct stat	file_infos;
	void 		run();
};
