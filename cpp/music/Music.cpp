#include "Music.hpp"
#include "tinyosc/tinyosc.h"
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

Music::Music(unsigned port)
{
	start_engine();
	open_connection(port);
}

void	Music::open_connection(unsigned port)
{
	if ((socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
	{
		perror("socket opening");
		error = 1;
	}
	else
	{
		memset((char *) &supercollider, 0, sizeof(supercollider));
		supercollider.sin_family = AF_INET;
		supercollider.sin_port = htons(port);
		supercollider.sin_addr.s_addr = htonl(INADDR_ANY);
		connect(socket_fd, (sockaddr*)&supercollider, sizeof(supercollider));
		error = 0;
	}
}

void	Music::send_osc(const char *address, float value)
{
	char buffer[1024];
	int len = tosc_writeMessage(buffer, sizeof(buffer),
								address, "f", value);
	send(socket_fd, buffer, len, 0);
}

void	Music::send_osc(unsigned index, const char *address, float value)
{
	char	buffer[1024];
	char	addr[100];
	int		len;

	sprintf(addr, "/%d_%s", index, address);
	len = tosc_writeMessage(buffer, sizeof(buffer), addr, "f", value);
	send(socket_fd, buffer, len, 0);
}

void	Music::send_osc(const char *address)
{
	char	buffer[1024];
	int		len;

	len = tosc_writeMessage(buffer, sizeof(buffer), address, "");
	send(socket_fd, buffer, len, 0);
}

void	Music::send_osc(const char *address, const char *format, ...)
{
	char buffer[1024];
	va_list ap;

	va_start(ap, format);
	const uint32_t len = tosc_vwrite(buffer, sizeof(buffer),
									 address, format, ap);
	va_end(ap);
	send(socket_fd, buffer, len, 0);
}
