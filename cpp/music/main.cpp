#include "Music.hpp"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

int go = 1;

void	handler(int sig)
{
	go = 0;
}

int		main (int argc, char *argv[], char *env[])
{
	Music	music(57120);

	signal(SIGINT, handler);
	music.send_osc("/2_amp", 0.f);
	music.send_osc("/2_amp", "s", "coucou");
	while(go)
		;
	return (0);
}
