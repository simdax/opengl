#include <thread>
#include "Music.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <signal.h>
#define xstr(a) str(a)
#define str(s) #s

Music::~Music()
{
	std::cout << "quitting SC\n";
	kill(pid + 1, SIGKILL);
	std::cout << pid;
}

void	Music::run_SC()
{
	int		sc;
	int		_pid;
	char	cmd[100];

	_pid = fork();
	if (!_pid)
	{
		sc = system(SC_PATH);
		if (sc == -1)
			perror("supercollider");
	}
	else
		pid = _pid;
}

void	Music::start_engine()
{
	std::thread sc(&Music::run_SC, this);
	sc.detach();
}
