#ifndef MUSIC_H
# define MUSIC_H

#include <arpa/inet.h>

class Music {
private:
	struct		sockaddr_in supercollider;
	int			socket_fd;
	void		open_connection(unsigned port);
	void		start_engine();
	void		run_SC();
	bool		opened = false;
	int			pid;
public:
	Music(unsigned port);
	~Music();
	void		send_osc(const char *address, float value);
	void		send_osc(const char *address);
	void		send_osc(unsigned index, const char *address, float value);
	void		send_osc(const char *address, const char *format, ...);
	int			error;
};

#endif
