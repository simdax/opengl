# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/scornaz/openGL/music/tinyosc/tinyosc.c" "/Users/scornaz/openGL/music/build/CMakeFiles/Music.dir/tinyosc/tinyosc.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "SC_PATH=\"/Users/scornaz/Downloads/SuperCollider/SuperCollider.app/Contents/MacOS/sclang -l SuperCollider/conf.yaml SuperCollider/test.scd\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/scornaz/openGL/music/Music.cpp" "/Users/scornaz/openGL/music/build/CMakeFiles/Music.dir/Music.cpp.o"
  "/Users/scornaz/openGL/music/engine.cpp" "/Users/scornaz/openGL/music/build/CMakeFiles/Music.dir/engine.cpp.o"
  "/Users/scornaz/openGL/music/main.cpp" "/Users/scornaz/openGL/music/build/CMakeFiles/Music.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SC_PATH=\"/Users/scornaz/Downloads/SuperCollider/SuperCollider.app/Contents/MacOS/sclang -l SuperCollider/conf.yaml SuperCollider/test.scd\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
