extern crate utils;

pub mod render_gl;
use render_gl::Shader;

pub struct Program{
    id: gl::types::GLuint,
}
impl Program{
    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }
    pub fn new(shaders: &[Shader]) -> Result<Program, String>
    {
                    let mut success: gl::types::GLint = 1;
            let mut len: gl::types::GLint = 0;
            let id: gl::types::GLuint;

        unsafe {
            id = gl::CreateProgram();
            for shader in shaders{
                gl::AttachShader(id, shader.id());
            }
            gl::LinkProgram(id);
            for shader in shaders{
                gl::DetachShader(id, shader.id());
            }
            gl::GetProgramiv(id, gl::LINK_STATUS, &mut success);
                gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
        }
        if success == 0 {
            let error = utils::create_whitespace_string_from_len(len as usize);
            unsafe {
                gl::GetProgramInfoLog(
                    id,
                    len,
                    std::ptr::null_mut(),
                    error.as_ptr() as *mut gl::types::GLchar,
                );
                return Err(error.to_string_lossy().into_owned());
            }
        }
        Ok(Program{ id })
    }
}
impl Drop for Program{
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}