extern crate utils;

    use gl;
    use std::ffi::{CStr};

    pub struct Shader {
        id: gl::types::GLuint,
    }
    impl Shader {
        pub fn id(&self) -> gl::types::GLuint {
            self.id
        }
        fn from_source(source: &CStr, kind: gl::types::GLuint) -> Result<Shader, String> {
            let mut success: gl::types::GLint = 1;
            let mut len: gl::types::GLint = 0;
            let id: gl::types::GLuint;

            unsafe {
                id = gl::CreateShader(kind);
                gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
                gl::CompileShader(id);
                gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut success);
                gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut len);
            }
            if success == 0 {
                let error = utils::create_whitespace_string_from_len(len as usize);
                unsafe {
                    gl::GetShaderInfoLog(
                        id,
                        len,
                        std::ptr::null_mut(),
                        error.as_ptr() as *mut gl::types::GLchar,
                    );
                    return Err(error.to_string_lossy().into_owned());
                }
            }
            Ok(Shader { id })
        }
    }
    impl Drop for Shader {
        fn drop(&mut self) {
            unsafe {
                gl::DeleteShader(self.id);
            }
        }
    }